#!/bin/sh

# This script is meant to test all kind of migrations of the new cacti postinst
# Just to be sure the final package behaves.

# Elbrus, Wed, 17 Apr 2013 20:04:32 +0200

set -e

backupDir=$(mktemp -d)
latestDeb=/var/cache/pbuilder/sid-amd64/result/cacti_0.8.8a+dfsg-6_all.deb
released=0.8.8a+dfsg-5

# MD5 checks
md5apold=aa7480c9c685128897b3ded391fc97e9
md5apnew=6702842953f7f7b8d8e58640f3da695f
md5apchanged=44ee43204cfdb37a55a8169972820bd4
md5liold=89db1c1adefc6f88053905271531a750
md5linew=ccbf1b3408e25365d34d1b6959823319
md5lichanged=566df5fd91cc27113cd0ac4babbbc3a4
md5empty=d41d8cd98f00b204e9800998ecf8427e
aPath=/etc/apache2/conf.d/cacti.conf
lPath=/etc/lighttpd/conf-available/20-cacti.conf

checkConfig() {
    return # Disabled for now, too much info, but needed for debugging
    echo
    echo "\033[01;34mThe configuration now looks like: \033[00m"
    ucfq cacti
    echo "/etc/cacti:"
    ls -al /etc/cacti/
    echo "/etc/apache2:"
    ls -al /etc/apache2
    if [ -d /etc/lighttpd ] ; then
	echo "/etc/lighttpd:"
	ls -al /etc/lighttpd
    fi

    if [ -d /etc/apache2/conf.d ] ; then
	echo "/etc/apache2/conf.d:"
	ls -al /etc/apache2/conf.d
    fi
    if [ -d /etc/apache2/conf-available ] ; then
	echo "/etc/apache2/conf-available:"
	ls -al /etc/apache2/conf-available
	if [ -f /etc/apache2/conf-available/cacti.conf ] ; then
	    echo -n "md5sum apache2: "
	    md5sum /etc/apache2/conf-available/cacti.conf
	fi
    fi
    if [ -d /etc/lighttpd/conf-available ] ; then
	echo "/etc/lighttpd/conf-available:"
	ls -al /etc/lighttpd/conf-available
	if [ -f /etc/lighttpd/conf-available/cacti.conf ] ; then
	    echo -n "md5sum lighttpd/cacti.conf: "
	    md5sum /etc/lighttpd/conf-available/cacti.conf
	fi
	if [ -f /etc/lighttpd/conf-available/20-cacti.conf ] ; then
	    echo -n "md5sum lighttpd/20-cacti.conf: "
	    md5sum /etc/lighttpd/conf-available/20-cacti.conf
	fi
    fi
}

restoreOrig () {
    echo ""
    echo "\033[01;34mDowngrading by installing cacti again\033[00m"
    rm -f /etc/lighttpd/conf-enabled/20-cacti.conf
    rm -f /etc/lighttpd/conf-available/cacti.conf
    rm -f /etc/lighttpd/conf-available/20-cacti.conf
    ucf --purge /etc/lighttpd/conf-available/20-cacti.conf
    ucfr --purge cacti /etc/lighttpd/conf-available/20-cacti.conf
    rm -rf /etc/apache2/conf-available
    ucf --purge /etc/apache2/conf-available/cacti.conf
    ucfr --purge cacti /etc/apache2/conf-available/cacti.conf
    rm -f /etc/apache2/conf.d/cacti.conf
    cd /etc/apache2/conf.d && ln -s ../../cacti/apache.conf cacti.conf && cd - || true
    apt-get --force-yes -y install cacti=$released
    cp -p $backupDir/* /etc/cacti/
    ucfq cacti
    invoke-rc.d apache2 reload || true
}

checkMD5() {
    if [ "X$expectAp" = "X" ] ; then
	if [ -e $aPath ] ; then 
	    echo "\033[01;31mFAILED\033[00m Apache conf MD5 check ($aPath)"
	else
	    echo "\033[01;32mPASSED\033[00m Apache conf MD5 check ($aPath)"
	fi
    else
	if [ $(md5sum $aPath | awk '{print $1}') = $expectAp ] ; then
	    if [ -h $aPath ] && [ "$(readlink $aPath)" = ../conf-available/cacti.conf ] ; then
		echo "\033[01;32mPASSED\033[00m Apache conf MD5 check (link $aPath)"
	    elif [ -f $aPath ] ; then
		echo "\033[01;32mPASSED\033[00m Apache conf MD5 check (file $aPath)"
	    else
		echo "\033[01;31mFAILED\033[00m Apache conf MD5 check ($aPath)"
	    fi
	else
	    echo "\033[01;31mFAILED\033[00m Apache conf MD5 check ($aPath)"
	fi
    fi
    if [ "X$expectLi" = "X" ] ; then
	if [ -e $lPath ] ; then 
	    echo "\033[01;31mFAILED\033[00m Lighttpd conf MD5 check ($lPath)"
	else
	    echo "\033[01;32mPASSED\033[00m Lighttpd conf MD5 check ($lPath)"
	fi
    else
	if [ $(md5sum $lPath | awk '{print $1}') = $expectLi ] ; then
	    if [ -h $lPath ] && [ "$(readlink $lPath)" = ../conf-available/cacti.conf ] ; then
		echo "\033[01;32mPASSED\033[00m Lighttpd conf MD5 check (link $lPath)"
	    elif [ -f $lPath ] ; then
		echo "\033[01;32mPASSED\033[00m Lighttpd conf MD5 check (file $lPath)"
	    else
		echo "\033[01;31mFAILED\033[00m Lighttpd conf MD5 check ($lPath)"
	    fi
	else
	    echo "\033[01;31mFAILED\033[00m Lighttpd conf MD5 check ($lPath)"
	fi
    fi
}

trap restoreOrig 0

echo "\033[01;34mMaking a copy of cacti configuration in $backupDir \033[00m"
cp -pR /etc/cacti/* $backupDir
checkConfig

echo "\033[01;34mInstalling $latestDeb \033[00m"
dpkg -i $latestDeb
expectAp=$md5apnew
expectLi=$md5linew

checkMD5
checkConfig
restoreOrig

echo "\033[01;34mInstalling $latestDeb with changes\033[00m"
echo "#Some comment to change md5sum" >> /etc/cacti/apache.conf
echo "#Some comment to change md5sum" >> /etc/cacti/lighttpd.conf
UCF_FORCE_CONFFOLD=true dpkg --install $latestDeb
expectAp=$md5apchanged
expectLi=$md5lichanged

checkMD5
checkConfig
restoreOrig

echo "\033[01;34mInstalling $latestDeb without\033[00m"
rm -f /etc/cacti/apache.conf
rm -f /etc/apache2/conf.d/cacti.conf
ucf --purge /etc/cacti/apache.conf
ucfr --purge cacti /etc/cacti/apache.conf
rm -f /etc/cacti/lighttpd.conf
rm -f /etc/lighttpd/conf-available/cacti.conf
ucf --purge /etc/cacti/lighttpd.conf
ucfr --purge cacti /etc/cacti/lighttpd.conf
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
expectAp=
expectLi=

checkMD5
checkConfig

echo "\033[01;34mInstalling $latestDeb again\033[00m"
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
expectAp=
expectLi=

checkMD5
checkConfig
restoreOrig

echo "\033[01;34mInstalling $latestDeb with target\033[00m"
mkdir -p /etc/apache2/conf-available
touch /etc/apache2/conf-available/cacti.conf
touch /etc/lighttpd/conf-available/20-cacti.conf
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
expectAp=$md5empty
expectLi=$md5empty

checkMD5
checkConfig
restoreOrig

echo "\033[01;34mInstalling $latestDeb with lighttpd 'enabled; no apache'\033[00m"
[ ! -h /etc/lighttpd/conf-available/cacti.conf ] && \
    ln -s ../../cacti/lighttpd.conf /etc/lighttpd/conf-available/cacti.conf
[ ! -h /etc/lighttpd/conf-enabled/cacti.conf ] && \
    ln -s ../conf-available/cacti.conf /etc/lighttpd/conf-enabled/cacti.conf
rm /etc/apache2/conf.d/cacti.conf
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
aPath=/etc/apache2/conf-available/cacti.conf
lPath=/etc/lighttpd/conf-enabled/20-cacti.conf
expectAp=$md5apold
expectLi=$md5liold

checkMD5
checkConfig
restoreOrig

echo "\033[01;34mInstalling $latestDeb with lighttpd 'enabled'; with apache\033[00m"
[ ! -h /etc/lighttpd/conf-available/cacti.conf ] && \
    ln -s ../../cacti/lighttpd.conf /etc/lighttpd/conf-available/cacti.conf
[ ! -h /etc/lighttpd/conf-enabled/cacti.conf ] && \
    ln -s ../conf-available/cacti.conf /etc/lighttpd/conf-enabled/cacti.conf
[ ! -h /etc/apache2/conf.d/cacti.conf ] && \
    ln -s ../../cacti/apache.conf /etc/apache2/conf.d/cacti.conf
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
aPath=/etc/apache2/conf.d/cacti.conf
expectAp=$md5apold
expectLi=$md5liold

checkMD5
checkConfig
# restoreOrig

echo "\033[01;34mDeleting $latestDeb \033[00m"
apt-get remove cacti
aPath=/etc/apache2/conf-available/cacti.conf
lPath=/etc/lighttpd/conf-available/20-cacti.conf
expectAp=$md5apold
expectLi=$md5liold

checkMD5
aPath=/etc/apache2/conf.d/cacti.conf
lPath=/etc/lighttpd/conf-enabled/20-cacti.conf
expectAp=
expectLi=

checkMD5
checkConfig

echo "\033[01;34mInstalling $latestDeb \033[00m"
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
expectAp=
expectLi=

checkMD5
checkConfig

echo "\033[01;34mInstalling $latestDeb with lighttpd 'enabled'; with apache\033[00m"
[ ! -h /etc/lighttpd/conf-enabled/20-cacti.conf ] && \
    ln -s ../conf-available/20-cacti.conf /etc/lighttpd/conf-enabled/20-cacti.conf
[ ! -h /etc/apache2/conf.d/cacti.conf ] && \
    ln -s ../conf-available/cacti.conf /etc/apache2/conf.d/cacti.conf
UCF_FORCE_CONFFOLD=true dpkg -i $latestDeb
aPath=/etc/apache2/conf.d/cacti.conf
expectAp=$md5apold
expectLi=$md5liold

checkMD5
checkConfig
# restoreOrig

exit 0
